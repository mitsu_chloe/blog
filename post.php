<?php

    //データベースユーザー
    $user = 'nanonano_test';
    $password = 'testpass';
    //利用するデータベース
    $dbName = 'nanonano_blog';
    //MySQLサーバ
    $host = 'mysql1.php.xdomain.ne.jp';
    //MySQLのDSN文字列
    $dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";

    //MySQLデータベースに接続する
    try {
    $pdo = new PDO($dsn,$user,$password);
    //プリペアドステートメントのエミュレーションを無効にする
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    //例外がスローされる設定にする
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(Exception $e) {
        echo '<span class="error">エラーがありました。</span><br>';
        echo $e->getMessage();
        exit();}

        //SQL文を作る
        $sql = "SELECT * FROM post";
        //プリペアードステートメントを作る
        $stm = $pdo->prepare($sql);
        //SQL文を実行する
        $stm->execute();
        //結果の取得（連想配列でうけとる）
        $posts = $stm->fetchAll(PDO::FETCH_ASSOC);

	$error = $title = $content = '';
	if (@$_POST['submit']){
        $no = $_POST['post_no'];
		$title = $_POST['title'];
		$content = $_POST['content'];
		if (!$title) $error .='タイトルがありません。<br>';
		if (mb_strlen($title) > 80) $error .='タイトルが長すぎます。<br>';
		if (!$content) $error .='本文がありません。<br>';
		if(!$error) {
			// $pdo = new PDO("mysql:dbname=blog", "root", "password");
			$st = $pdo ->query("INSERT INTO post(no, title, content) VALUES('$no', '$title', '$content')");
			header('Location: index.php');
			exit();
		}
	}
	require 't_post.php';
?>
<?php

//画像を保存するディレクトリー
$dir="image/";
//日本語を省くための正規表現
$pattern="/^[a-z0-9A-Z\-_]+\.[a-zA-Z]{3}$/";



//リクエストがPOSTかどうかチェック
if($_SERVER["REQUEST_METHOD"]=="POST" && !empty ($_POST)){
	
	//フォームボタンのvalueを取得する
    $action=$_POST["action"];

	/*-------------------------------------------------------
	   アップロードする処理
	--------------------------------------------------------*/
    if($action=="upload"){
	
	
	//アップロードされたファイルを取得
	$upfile=$_FILES["image"]["name"];
	
	//ファイルがアップロードされたかチェックする
	if(!empty ($upfile)){
	
			
		
		//ファイル名に日本語が入ってるかチェック
        if(!preg_match($pattern,$upfile)){
           $er["jp"]="日本語はダメ";
        }
		
		//アップされた画像の拡張子を抜き出す
		$ext=substr($upfile,-3);
		
		//拡張子を調べて画像のアップ
        if($ext!="jpg" && $ext!="gif" && $ext!="png"){
            $er["image"]="拡張子がjpgとgifとpngのみアップできます";
        }else{
		
		//ファイル重複チェックするためにディレクトリー内のファイルを取得する
        $filelist=scandir($dir);
        
		foreach($filelist as $file){
         
		 //is_dir関数でディレクトリー以外のファイル（つまり画像のみ）を調べる
		 if(!is_dir($file)){
             if($upfile==$file){
                $er["double"]="重複してるのでアップできません。";
             }
         }
        }
		
        }
		
		//エラーの配列をチェックして空だった場合・・つまりエラーがなければ画像をアップロードする
		if(empty ($er)){
		   move_uploaded_file($_FILES["image"]["tmp_name"],$dir.$upfile);
		}
	
    }
	
	
    
	/*-------------------------------------------------------
	   削除する処理
	--------------------------------------------------------*/
	
	//削除ボタンが押された場合
    }elseif($action=="delete"){
		
		//チェックされたファイル名を取得
        $deletefiles=$_POST["deletefile"];
        //ファイルがアップロードされたかチェックする
		if(!empty ($deletefiles)){
		//チェックされた画像の数だけforeachでぶん回す
		//ファイルが実際に存在していた場合にunlink関数で画像ファイルを削除する
           foreach($deletefiles as $dfile){
               if(file_exists($dir.$dfile)){
                   unlink($dir.$dfile);
               }
           }
		   
		   }
			
    }
	
	
}

?>
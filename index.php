<?php

    // require 't_index.php';

    //データベースユーザー
    $user = 'nanonano_test';
    $password = 'testpass';
    //利用するデータベース
    $dbName = 'nanonano_blog';
    //MySQLサーバ
    $host = 'mysql1.php.xdomain.ne.jp';
    //MySQLのDSN文字列
    $dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";

    //MySQLデータベースに接続する
    try {
    $pdo = new PDO($dsn,$user,$password);
    //プリペアドステートメントのエミュレーションを無効にする
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    //例外がスローされる設定にする
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(Exception $e) {
        echo '<span class="error">エラーがありました。</span><br>';
        echo $e->getMessage();
        exit();
    }

    ?>
    <!DOCTYPE html>
    <html lang="ja">
    <head>
    <meta charset="utf-8">
    <title>BLOG</title>
    <link rel="stylesheet" href="blog.css">
    </head>
    <body>
    <div>
        <?php
        //SQL文を作る
        $sql1 = "SELECT * FROM post ORDER BY no DESC";
        //プリペアードステートメントを作る
        $stm1 = $pdo->prepare($sql1);
        //SQL文を実行する
        $stm1->execute();
        //結果の取得（連想配列でうけとる）
        $posts = $stm1->fetchAll(PDO::FETCH_ASSOC);



        //SQL文を作る
        $sql2 = "SELECT * FROM comment";
        //プリペアードステートメントを作る
        $stm2 = $pdo->prepare($sql2);
        //SQL文を実行する
        $stm2->execute();
        //結果の取得（連想配列でうけとる）
        $comments = $stm2->fetchAll(PDO::FETCH_ASSOC);

        $time = date("Y/m/d H:i:s");
        ?>
    <h1>BLOG</h1>
    <p class="new_post"><a href="post.php">新規投稿</a></p>

<?php foreach ($posts as $post) { ?>
  <div class="post">
    <h2><?php echo $post['title'] ?></h2>
    <p><?php echo $post['no'] ?></p>
    <p><?php echo nl2br($post['content']) ?></p>
    <p class="post_time">投稿日：<?php echo $post['time'] ?></p>

      <div class="comment">
      <?php foreach ($comments as $comment) { ?>
        <?php if($comment['post_no'] == $post['no']){ ?>
          <h3><?php echo $comment['name'], "さん" ?></h3>
          <p><?php echo nl2br($comment['content']) ?></p>
          <p><?php echo nl2br($comment['time']) ?></p> 
           <?php } ?>
      <?php } ?>
      </div>
            <p class="commment_link">
      投稿日：<?php echo $post['time'] ?>
      <a href="comment.php?no=<?php echo $post['no'] ?>">コメント</a>
    </p>
    </div>
<?php } ?>
    </div>
    </body>
    </html>

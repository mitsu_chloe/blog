<?php
// XSS対策のためのHTMLエスケープ
function es($data, $charset='utf-8'){
	//$dateが配列したとき
	if (is_array($data)){
		//再帰予び出し
		return array_map(__METHOD__, $data);
	} else {
		//HTMLエスケープを行う
		return htmlspecialchars($data, ENT_QUOTES, $charset);
	}
}

//配列の文字エンコードのチェックを行う
function cken(array $data){
	$result = true;
	foreach ($data as $key => $value) {
		if (is_array($value)){
			//含まれている値が配列の時文字列に連結する
			$value = implode("", $value);
		}
		if (!mb_check_encoding($value)){
			//文字コードが一致しないとき
			$result = false;
			//foreachでの走査をブレイクする
			break;
		}
	}
	return $result;
}

// ?>